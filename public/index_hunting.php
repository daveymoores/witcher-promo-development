<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>The Witcher Film Club - Vote for your favourite hunting movie in association with IGN</title>

    <!-- <meta property="og:title" content="The Evil Within">
    <meta property="og:type" content="article">
    <meta property="og:site_name" content="The Evil Within">
    <meta property="og:url" content="http://uk-microsites.ign.com/theevilwithin/">
    <meta property="og:description" content="Vote for your favourite horror movie in association with IGN">
    <meta property="og:image" content="">
    <meta property="fb:admins" content="546507370"> -->

    <link rel="shortcut icon" href="favicon.ico">
    <link rel=apple-touch-icon href="apple-touch-icon.png">
    <link rel=apple-touch-icon sizes=72x72 href="apple-touch-icon-72x72.png">
    <link rel=apple-touch-icon sizes=114x114 href="apple-touch-icon-114x114.png">


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.3.15/slick.css"/>
    <link rel="stylesheet" href="stylesheets/app.css" />

    <script src="bower_components/modernizr/modernizr.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="//use.typekit.net/nwm4qph.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

    <script src="js/hide_it.js"></script>

  </head>


<body>


 <script src="js/output_age_form.js"></script>

<!--uncomment for live-->
<div id="policyNotice" style="display:none">
    <div class="close-btn">
        <a href="#_" title="Close" data-domain=".ign.com">Close</a>
    </div>
    <p>We use cookies and other technologies to improve your online experience. By using this Site, you consent to this use as described in our <a href="http://corp.ign.com/policies/cookie-eu" target="_blank">Cookie Policy</a>.</p>
</div>
<!--uncomment for live-->

    <!--off canvas menu-->
  <div class="off-canvas-wrap" data-offcanvas>
    <div class="inner-wrap">
      <nav class="tab-bar top" data-topbar role="navigation">

          <section class="left-small show-for-small-only">
              <a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
          </section>

          <div class="row">

            <div class="ign_red-wrapper ">
              <h1 class="title"><a href="http://ign.com"><img src="img/ign-logo.png" alt="IGN logo"></a></h1>
            </div>

            <ul class="left share_btn" id="share_override">
              <li><a href="#"></a></li>
            </ul>

          </div>

      </nav>

      <aside class="left-off-canvas-menu show-for-small-only">
         <ul class="off-canvas-list">
           <li><label>Witcher 3</label></li>
               <li><a href="#_" class="choose">Vote now</a></li>
               <li><a href="#_" class="join">Join the screening</a></li>
               <li><a href="#_" class="about">About the game</a></li>
               <li><a href="index_monster.php">Monster movie vote</a></li>
               <li><a href="http://buy.thewitcher.com/" target="_blank" class="preorder" onClick="_gaq.push(['_trackEvent', 'pre-order', 'click', 'pre-order in navigation']);">pre-order the game</a></li>
         </ul>
      </aside>
      <!--end off canvas menu-->

      <header>
        <div class="secondary nav" id="stick">
          <ul>
              <li><a href="#_" class="choose">vote now</a></li>
              <li><a href="#_" class="join">Join the screening</a></li>
              <li><a href="#_" class="about">about the game</a></li>
              <li><a href="http://buy.thewitcher.com/" target="_blank" class="preorder" onClick="_gaq.push(['_trackEvent', 'pre-order', 'click', 'pre-order in navigation']);">pre-order the game</a></li>
          </ul>
        </div>

        <!--movie bar-->
        <div class="movie_bar">
           <ul>
             <li><a href="index_monsters.php"><img src="img/monster-text.png" alt="monster"></a></li>
             <li><a href="index_hunting.php"><img src="img/hunting-text.png" alt="hunting"></a></li>
             <li><a href="#_"><img src="img/screening-text.png" alt="screening"></a></li>
           </ul>
        </div>
        <!--end movie bar-->

        <div class="header_wrapper row">


          <div class="small-12 columns show-for-small-only">
            <div class="flex-video">
              <iframe width="320" height="315" src="//www.youtube.com/embed/FcogCjLymeI?rel=0" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>

          <div class="small-7 columns show-for-small-only"><img class="evilLogo" src="img/witcher_logo.png" alt="witcher 3 logo"></div>
          <div class="small-5 columns show-for-small-only">
            <div class="pre-order_wrapper">
              <a href="http://buy.thewitcher.com/" onClick="_gaq.push(['_trackEvent', 'pre-order', 'click', 'pre-order in hero']);"></a>
            </div>
          </div>


          <div class="large-7 medium-7 small-12 columns">

            <div class="hide-for-small">

              <img class="evilLogo" src="img/witcher_logo_new.png" alt="witcher 3 logo">

              <img class="hashtag" src="img/hashtag.png" alt="#thewitcherfilmclub">

              <div class="flex-video">
                <iframe width="320" height="315" src="//www.youtube.com/embed/FcogCjLymeI?rel=0" frameborder="0" allowfullscreen></iframe>
              </div>

              <div class="pre-order_wrapper" id="buy" onClick="_gaq.push(['_trackEvent', 'pre-order', 'click', 'pre-order in hero']);">
                <a href="http://buy.thewitcher.com/"></a>
              </div>

              <div class="clearfix"></div>


              <img id="signup" src="img/sign_up.png">
              <img class="evilText" src="img/header_text-hunting.png" alt="Choose your favourite monster movie and be in with a chance of seeing it on the big screen">

            </div>

            <img class="evilText show-for-small-only" src="img/header_text-hunting.png" alt="Choose your favourite monster movie and be in with a chance of seeing it on the big screen">

          </div>
        </div>
      </header>



      <div ng-app="EvilApp">
        <section class="main-section" id="movies" ng-controller="AppCtrl as app">
          <div class="row" data-equalizer>

            <div class="large-3 medium-6 small-12 columns flip-container" data-equalizer-watch>
              <div class="flipper">
                <div class="front">
                  <div class="blud_wrapper">
                    <img src="img/posters/deer_hunter.jpg" alt="placeholder">
                  </div>
                  <div class="voting_panel">
                    <p vote-directive activate="selected" data-title="Deer Hunter">Vote</p>
                    <div class="check"><span></span></div>
                  </div>
                </div><!--front-->

                <div class="back">

                    <p>CONFIRM<br> YOUR VOTE?</p>
                    <div class="button yes" id="submit" data-reveal-id="confirm_modal">YES</div>
                    <div class="button no" id="no" spinback-directive>NO</div>

                </div><!--back-->
              </div><!--flipper-->
            </div><!--flip container-->

            <div class="large-3 medium-6 small-12 columns flip-container" data-equalizer-watch>
              <div class="flipper">
                <div class="front">
                  <div class="blud_wrapper">
                    <img src="img/posters/furnace.jpg" alt="placeholder">
                  </div>
                  <div class="voting_panel">
                    <p vote-directive activate="selected" data-title="Pan's Labyrinth">Vote</p>
                    <div class="check"><span></span></div>
                  </div>
                </div><!--front-->

                <div class="back">

                    <p>CONFIRM<br> YOUR VOTE?</p>
                    <div class="button yes" id="submit" data-reveal-id="confirm_modal">YES</div>
                    <div class="button no" id="no" spinback-directive>NO</div>

                </div><!--back-->
              </div><!--flipper-->
            </div><!--flip container-->

            <div class="large-3 medium-6 small-12 columns flip-container" data-equalizer-watch>
              <div class="flipper">
                <div class="front">
                  <div class="blud_wrapper">
                    <img src="img/posters/hunger_games.jpg" alt="placeholder">
                  </div>
                  <div class="voting_panel">
                    <p vote-directive activate="selected" data-title="Alien">Vote</p>
                    <div class="check"><span></span></div>
                  </div>
                </div><!--front-->

                <div class="back">

                    <p>CONFIRM<br> YOUR VOTE?</p>
                    <div class="button yes" id="submit" data-reveal-id="confirm_modal">YES</div>
                    <div class="button no" id="no" spinback-directive>NO</div>

                </div><!--back-->
              </div><!--flipper-->
            </div><!--flip container-->

            <div class="large-3 medium-6 small-12 columns flip-container" data-equalizer-watch>
              <div class="flipper">
                <div class="front">
                  <div class="blud_wrapper">
                    <img src="img/posters/running_man.jpg" alt="placeholder">
                  </div>
                  <div class="voting_panel">
                    <p vote-directive activate="selected" data-title="Dracula">Vote</p>
                    <div class="check"><span></span></div>
                  </div>
                </div><!--front-->

                <div class="back">

                    <p>CONFIRM<br> YOUR VOTE?</p>
                    <div class="button yes" id="submit" data-reveal-id="confirm_modal">YES</div>
                    <div class="button no" id="no" spinback-directive>NO</div>

                </div><!--back-->
              </div><!--flipper-->
            </div><!--flip container-->

          </div>

         <div class="row" data-equalizer>

            <div class="large-3 medium-6 small-12 columns flip-container" data-equalizer-watch>
              <div class="flipper">
                <div class="front">
                  <div class="blud_wrapper">
                    <img src="img/posters/predator-poster-artwork-arnold-schwarzenegger-carl-weathers-elpidia-carrillo.jpg" alt="placeholder">
                  </div>
                  <div class="voting_panel">
                    <p vote-directive activate="selected" data-title="Hellboy">Vote</p>
                    <div class="check"><span></span></div>
                  </div>
                </div><!--front-->

                <div class="back">

                    <p>CONFIRM<br> YOUR VOTE?</p>
                    <div class="button yes" id="submit" data-reveal-id="confirm_modal">YES</div>
                    <div class="button no" id="no" spinback-directive>NO</div>

                </div><!--back-->
              </div><!--flipper-->
            </div><!--flip container-->

            <div class="large-3 medium-6 small-12 columns flip-container" data-equalizer-watch>
              <div class="flipper">
                <div class="front">
                  <div class="blud_wrapper">
                    <img src="img/posters/terminator.jpg" alt="placeholder">
                  </div>
                  <div class="voting_panel">
                    <p vote-directive activate="selected" data-title="District 9">Vote</p>
                    <div class="check"><span></span></div>
                  </div>
                </div><!--front-->

                <div class="back">

                    <p>CONFIRM<br> YOUR VOTE?</p>
                    <div class="button yes" id="submit" data-reveal-id="confirm_modal">YES</div>
                    <div class="button no" id="no" spinback-directive>NO</div>

                </div><!--back-->
              </div><!--flipper-->
            </div><!--flip container-->

            <div class="large-3 medium-6 small-12 columns flip-container" data-equalizer-watch>
              <div class="flipper">
                <div class="front">
                  <div class="blud_wrapper">
                    <img src="img/posters/terminator_2.jpg" alt="placeholder">
                  </div>
                  <div class="voting_panel">
                    <p vote-directive activate="selected" data-title="Predator">Vote</p>
                    <div class="check"><span></span></div>
                  </div>
                </div><!--front-->

                <div class="back">

                    <p>CONFIRM<br> YOUR VOTE?</p>
                    <div class="button yes" id="submit" data-reveal-id="confirm_modal">YES</div>
                    <div class="button no" id="no" spinback-directive>NO</div>

                </div><!--back-->
              </div><!--flipper-->
            </div><!--flip container-->

            <div class="large-3 medium-6 small-12 columns flip-container" data-equalizer-watch>
              <div class="flipper">
                <div class="front">
                  <div class="blud_wrapper">
                    <img src="img/posters/avatar.jpg" alt="placeholder">
                  </div>
                  <div class="voting_panel">
                    <p vote-directive activate="selected" data-title="The Monster Squad">Vote</p>
                    <div class="check"><span></span></div>
                  </div>
                </div><!--front-->

                <div class="back">

                    <p>CONFIRM<br> YOUR VOTE?</p>
                    <div class="button yes" id="submit" data-reveal-id="confirm_modal">YES</div>
                    <div class="button no" id="no" spinback-directive>NO</div>

                </div><!--back-->
              </div><!--flipper-->
            </div><!--flip container-->

          </div>


            <div class="row">
              <div class="large-8 large-centered columns">
                <p class="results">RESULTS WILL BE RELEASED ON THE 12TH OF MAY</p>
              </div>
            </div>


        </section>
      </div>

      <section class="game" id="gameContent">
        <div class="aboutthegame">
            <div class="row">

              <div class="large-4 columns">

              </div>

              <div class="large-8 columns">
                <h2>A Gigantic and Limitless Open<br> World to Explore</h2>
                <p>Sail through open seas, dive in search of long lost artifacts or gallop your mount into combat—the world of the Witcher beckons to be explored. Visit the luxurious and corrupt city of Novigrad, face the frigid cold of the wind-swept Skellige islands, and challenge the dark forces roaming the forlorn, war-ravaged No Man’s Land. While on the road, use your special witcher senses to hunt down ferocious monsters and earn gold. Do anything you want, anytime you want!</p>

                <div id="slider">
                  <div><img src="img/screens/The_Witcher_3_Wild_Hunt-Crossroads_1407869449.jpg"></div>
                  <div><img src="img/screens/The_Witcher_3_Wild_Hunt-Geralt_torching_his_enemies_1407869453.jpg"></div>
                  <div><img src="img/screens/The_Witcher_3_Wild_Hunt-Johny_the_Godling_1407869456.jpg"></div>
                  <div><img src="img/screens/The_Witcher_3_Wild_Hunt-Three_Witches_1407869458.jpg"></div>
                  <div><img src="img/screens/The_Witcher_3_Wild_Hunt-Witch_1407869459.jpg"></div>
                  <div><img src="img/screens/The_Witcher_3-Wild_Hunt_Dialogue_sequences_feature_new_dynamic_camera.jpg"></div>
                  <div><img src="img/screens/The_Witcher_3-Wild_Hunt_Geralt_can_take_care_of_himself_in_any_situation.jpg"></div>
                  <div><img src="img/screens/The_Witcher_3-Wild_Hunt_Skellige__is_a_beautiful_place.jpg"></div>
                  <div><img src="img/screens/The_Witcher_3-Wild_Hunt_Various_types_of_enemies_require_different_approach.jpg"></div>
                </div>

              </div>

          </div>
        </div>
      </section>

        <section class="comp" id="comp">

          <div class="comp_wrapper row">
            <div class="large-5 columns">
              <h2>Enter your details to be notified when ticket booking goes live.</h2>

               <p>We’ll be screening the film with the most votes at Vue cinemas in Birmingham, Edinburgh, Manchester and London on 12th May, tickets will be available on a first come, first served basis so you’ll need to be quick!<p>
            </div>
            <div class="large-6 large-offset-1 columns">

                <form method="post" class="form-horizontal" data-abide="ajax" id="main_form">

                  <!-- thankyou message shown on successfull db entry :) -->
                  <div class="thankyou-modal" style="display:none;">
                    <p>Thanks for your entry, we'll email you when tickets are available.</p>
                    <p>For a chance to be at our monster movie screening, <b><a href="index_monsters.php">click here</a></b></p>
                  </div>

                  <h2>Your Details</h2>
                  <div class="row">
                    <div class="large-6 columns">
                      <label>FIRST NAME

                        <input type="text" class="forminput" required pattern="[a-zA-Z]+" name="fname" value="<?php if(isset($_POST['fname'])){echo $_POST['fname'];}?>">
                      </label>
                      <small class="error">First name is required</small>
                    </div>
                    <div class="large-6 columns">
                      <label>LAST NAME
                        <input type="text" class="forminput" required pattern="[a-zA-Z]+" name="lname" value="<?php if(isset($_POST['lname'])){echo $_POST['lname'];}?>">
                      </label>
                      <small class="error">Last name is required</small>
                    </div>
                  </div>

                  <div class="row">
                    <div class="large-12 columns">
                      <label>EMAIL
                        <input type="email" class="forminput" type="email" required name="email" value="<?php if(isset($_POST['email'])){echo $_POST['email'];}?>">
                      </label>
                      <small class="error">Email is required</small>
                    </div>
                  </div>

                  <div class="row">
                    <div class="large-12 columns">
                      <div class="row pushDown">
 <!--                        <div class="large-1 column">
                          <input id="checkbox1" type="checkbox" name="terms" value="1">
                        </div>
                        <div class="large-11 column">
                          <label class="checkboxLabel" for="checkbox1">I confirm that I am a UK resident over the age of 18 and have read and understood the <a href="#_">terms and conditions and the privacy policy.</a></label>
                        </div> -->
                      </div>

                        <div class="row">
                          <div class="large-1 medium-1 small-1 column">
                            <input id="checkbox2" type="checkbox" name="subscribe" value="1">
                          </div>
                          <div class="large-11 medium-11 small-11 column">
                            <label class="checkboxLabel" required for="checkbox2">I agree that IGN can use my email address to send me a notification when tickets for the screening event are available to be booked.</label>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="large-12 columns">
                        <input type="hidden" name="_captcha" value="">
                        <input type="hidden" name="_form" value="lower">
                        <input type="submit" value="SUBMIT" class="button submit_btn" name="_submit" ng-submit>
                      </div>
                    </div>
                  <!-- </div> -->
                </form>
            </div>
          </div>
        </section>

        <!--start logo garden-->
        <div class="logo_garden">
          <div class="row">
            <div class="large-12">
              <img src="img/logo_garden.png" alt="logos">
              <p class="sml_text">The Witcher® is a trademark of CD PROJEKT S. A. The Witcher game © CD PROJEKT S. A. All rights reserved. The Witcher game is based on a novel of Andrzej Sapkowski. All other copyrights and trademarks are the property of their respective owners.</p>
            </div>
          </div>
        </div>
        <!--end logo garden-->

        <footer class="tab-bar">
          <div class="row">

            <div class="ign_red-wrapper">
              <div class="title"><img src="img/ign_ent.png" alt="IGN Entertainment logo"></div>

              <ul>
                <li><a href="http://corp.ign.com/privacy.html" title="Privacy Policy">Privacy policy</a></li>
                <li><a href="http://corp.ign.com/user-agreement.html" title="User Agreement">User agreement</a></li>
              </ul>
            </div>

            <ul class="left links">
              <ul>
                <li><p>Copyright 2014<br>IGN Entertainment UK, Inc.</p></li>
                <li><a href="http://uk.corp.ign.com/#about" title="About Us">About Us</a></li>
                <li><a href="http://uk.corp.ign.com/#contact" title="Contact Us">Contact Us</a></li>
                <li><a href="http://corp.ign.com/feeds.html" title="RSS Feeds">RSS Feeds</a></li>
              </ul>
            </ul>

          </div>
        </footer>

      <a class="exit-off-canvas"></a>

    </div>
  </div>


<div id="start_modal" class="reveal-modal small">
    <p>Please confirm you are 18 years of age or over, if you are not please <a href="http://ign.com">go back to IGN</a></p>
</div>

<div id="confirm_modal" class="reveal-modal medium" data-reveal>
  <h2>Thank You for voting</h2>
  <p class="lead">We’ll be screening the film with the most votes at cinemas in Birmingham, Bristol, Edinburgh, Glasgow, Manchester and London on 12th May, tickets will be available on a first come, first served basis so you’ll need to be quick!</p>

  <form method="post" class="form-horizontal" id="modal_form" data-abide="ajax">

    <!-- thankyou message shown on successfull db entry :) -->
    <div class="thankyou-modal" style="display:none;">
      <p>Thanks for your entry, we'll email you when tickets are available.</p>
      <p>For a chance to be at our monster movie screening, <b><a href="index_monsters.php">click here</a></b></p>
    </div>

    <div class="row">
      <div class="large-6 columns">
        <label>FIRST NAME

          <input type="text" class="forminput" required pattern="[a-zA-Z]+" name="fname" value="<?php if(isset($_POST['fname'])){echo $_POST['fname'];}?>">
        </label>
        <small class="error">First name is required</small>
      </div>
      <div class="large-6 columns">
        <label>LAST NAME
          <input type="text" class="forminput" required pattern="[a-zA-Z]+" name="lname" value="<?php if(isset($_POST['lname'])){echo $_POST['lname'];}?>">
        </label>
        <small class="error">Last name is required</small>
      </div>
    </div>

    <div class="row">
      <div class="large-12 columns">
        <label>EMAIL
          <input type="email" class="forminput" type="email" required name="email" value="<?php if(isset($_POST['email'])){echo $_POST['email'];}?>">
        </label>
        <small class="error">Email is required</small>
      </div>
    </div>

    <div class="row">
      <div class="large-12 columns">
        <div class="row pushDown">
<!--                        <div class="large-1 column">
            <input id="checkbox1" type="checkbox" name="terms" value="1">
          </div>
          <div class="large-11 column">
            <label class="checkboxLabel" for="checkbox1">I confirm that I am a UK resident over the age of 18 and have read and understood the <a href="#_">terms and conditions and the privacy policy.</a></label>
          </div> -->
        </div>

          <div class="row">
            <div class="large-1 medium-1 small-1 column">
              <input id="checkbox2" type="checkbox" name="subscribe" value="1">
            </div>
            <div class="large-11 medium-11 small-11 column">
              <label class="checkboxLabel" required for="checkbox2">Please check here if you consent to receiving a one-off email from IGN alerting you to the fact that tickets are available to book. Note that receipt of an alert email is not a guarantee of access to the event.</label>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="large-12 columns">
          <input type="hidden" name="_captcha" value="">
          <input type="hidden" name="_form" value="modal">
          <input type="submit" value="SUBMIT AND CONTINUE" class="button submit_btn" name="_submit" ng-submit>
          <input type="submit" value="RETURN TO IGN" class="button right" name="" ng-submit>
        </div>
      </div>
    <!-- </div> -->
  </form>
  <a class="close-reveal-modal">&#215;</a>
</div>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular.min.js"></script>

    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/share.min.js"></script>
    <script src="js/ageChecker.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/waypoints-sticky.min.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.3.15/slick.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/vote.js"></script>

    <script>

      (function(){
          var cookieName = 'persist-policy';
          var cookieValue = 'eu';
          var createCookie = function(name,value,days,domain) {
              var expires = '';
              var verifiedDomain = '';
              if (days) {
                  var date = new Date();
                  date.setTime(date.getTime()+(days*24*60*60*1000));
                  expires = '; expires='+date.toGMTString();
              }
              if (domain) {
                  verifiedDomain = '; domain='+domain;
              }
              document.cookie = name+'='+value+expires+verifiedDomain+'; path=/';
          };
          var readCookie = function(name) {
              var nameEQ = name + "=";
              var ca = document.cookie.split(';');
              for(var i=0;i < ca.length;i++) {
                  var c = ca[i];
                  while (c.charAt(0)==' ') c = c.substring(1,c.length);
                  if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
              }
              return null;
          };

          window.initPolicyWidget = function(){
              jQuery('#policyNotice').show().find('.close-btn a').click(function(e){
                  createCookie(cookieName, cookieValue, 180, jQuery(this).data('domain'));
                  jQuery('#policyNotice').remove();
                  return false;
              });
          }
          var cookieContent = readCookie(cookieName);
          if (typeof  cookieContent === 'undefined' || cookieContent != cookieValue) {
              jQuery(document).ready(initPolicyWidget);
              jQuery(document).trigger('policyWidgetReady');
          }
      })();

  </script>


  <!--begin GA script-->
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-15279170-1']);
    _gaq.push(['_trackPageview', 'witcher_tracking']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

  </script>
  <!--end GA script-->

  <!-- Begin comScore Tag -->
  <!--script>
    var _comscore = _comscore || [];
    _comscore.push({ c1: "2", c2: "3000068" });
    (function() {
      var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
      s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
      el.parentNode.insertBefore(s, el);
    })();
  </script>
  <noscript>
    <img src="http://b.scorecardresearch.com/p?c1=2&c2=3000068&cv=2.0&cj=1" />
  </noscript-->
   <!-- End comScore Tag -->

  </body>
</html>
